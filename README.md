README
=========

---

Personal Details
===
- ** Student Name **: Luke Taylor
- ** Student Number **: 10420226
- ** Course Title **: Computing & Games Development
- ** Module Code **: AINT209
- ** Blog Link **: https://lowflunky.wordpress.com/category/animation/
- ** Youtube Link **: https://youtu.be/tAExvaw17Jg

----

Project Details
===
Space Animation!

Final Assignment designed to implement lessons learned in previous Dark Arts of
C# lesson, but also newly taught material on animations and particle effects.

The game is based around a simple mechanic, kill the other player before they kill
you. You are both trapped in a strange spacecraft/part of a space station and all
you know is someone has to be killed.

Floating Debris, broken doors and lights with gunfire.

The project is going to utilize the Game Entity and Pooling methods taught by
Richard Weeks and implement basic animations on the character using bipedal models
provided by Paul Watson to give some more life to the players.

----

Credits
===
- <<ASSETS USED>>
- Unity Character Controller - Unity Default Asset
- Assistance editing Character Controller - http://answers.unity3d.com/questions/318766/use-gamepad-right-analog-stick-instead-of-mouse-to.html
- Menu Music "Into The Fringe" - http://www.newgrounds.com/audio/listen/174891
- Game Music "{IDR}-Painted Void" - http://www.newgrounds.com/audio/listen/195792
- Reload Sound - https://www.freesound.org/people/martian/sounds/182229/
- Gunshot Sound - https://www.freesound.org/people/Xenonn/sounds/128301/
- Footstep Sound - https://www.freesound.org/people/Samulis/sounds/197779/
- Moonhouse Font - http://www.dafont.com/moonhouse.font
- DeepSpaceRed Skybox - https://www.assetstore.unity3d.com/en/#!/content/3392
- Military_Rifle_01_Free_V2 (Including MotusMan_v2) - Paul Watson
- Container Barrel and Boxes - Paul Watson

- <<PEOPLE TO CREDIT>>
- Marius Varga - Programming assistance and teaching.
- Paul Watson - Digital Art and Animation teaching and guidance
- Richard Weeks - C# Teaching and Dark Arts for Pooling
- James King & Jerry Tao - Fresh pairs of eyes on buggy code, finding missing
semicolons etc.
