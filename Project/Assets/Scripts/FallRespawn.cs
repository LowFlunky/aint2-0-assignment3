﻿using UnityEngine;
using System.Collections;

/*
 * Author: Luke Taylor
 * Purpose: Spawn player back in the station if they fall into space.
 */ 
public class FallRespawn : MonoBehaviour 
{
    // Spawn Location
    public GameObject spawnLocation;

    // If a player collides with this trigger they will be moved back inside
    void OnTriggerEnter(Collider col)
    {
        if (col.collider.tag == "Player" || col.collider.tag == "Player2")
        {
            col.transform.position = spawnLocation.transform.position;
            col.transform.rotation = spawnLocation.transform.rotation;
        }
    }
}
