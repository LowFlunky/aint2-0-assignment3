﻿using UnityEngine;
using System.Collections;

/*
 * Author: Luke Taylor & Richard Weeks from C# Dark Arts
 * Purpose: Implement the GameEntity and Pooling systems into a firing mechanic
 */
public class Bullet : GameEntity 
{
    // Gunshot sounds
    public AudioSource shotSound;

    //Force and position variables assigned by the direction of fire in the Player script.
    private Vector3 spawnPos;
    private Vector3 spawnForce;
    //The "Parent" Game Entity to be used when assigning the heirachy (Currently thinks it is not being used, yet it is for the Pooler)
    private GameEntity entityParent;

    // Called when the Player fires.
    public static void Create(GameEntity Entity, GameObject Prefab, Vector3 position, Vector3 force, Transform rot)
    {
        // Create an object from the prefab passed in
        GameObject NewBullet = ObjectPooler.Create(Prefab);

        // Create an entity of type Bullet with all of the prefabs components.
        Bullet BulletEntity = NewBullet.GetComponent<Bullet>();
        // Assign the parent for the hiearchy view
        BulletEntity.entityParent = Entity;
        // Set transform, force and initilaise the bullet.
        BulletEntity.SetTransform(position);
        //BulletEntity.transform.rotation = rot.rotation;
        BulletEntity.SetForce(force, rot);
        BulletEntity.Initialise();
    }

    // Call Initialise
	void Start () 
    {
        Initialise();
	}


    // On Intilisation set the position and force, called by the fire direction, and start the bullets life timer.
    public override void Initialise()
    {
        base.Initialise();

        // Ensure that its "current" velocity and angular velocity are 0 else bullets appear drunk when reused.
        this.rigidbody.velocity = Vector3.zero;
        this.rigidbody.angularVelocity = Vector3.zero;
        transform.position = spawnPos;
        this.rigidbody.AddForce(spawnForce);
        shotSound.Play();
        StartCoroutine("Timer");
    }


    // Set Force.
    public void SetForce(Vector3 force, Transform rot)
    {
        spawnForce = force * 3000;
    }

    // Set Transform.
    public void SetTransform(Vector3 position)
    {
        spawnPos = position;
    }

    //Start death on Collisions and Triggers
    void OnCollisionEnter(Collision col)
    {
        TriggerDeath();
    }

    void OnCollisionExit(Collision col)
    {
        TriggerDeath();
    }
    
    // If hitting anything Trigger Death
    void OnTriggerEnter(Collider col)
    {
        TriggerDeath();
    }

    // After 2 seconds without hitting anything Trigger Death (So the bullet doesn't travel through space forever)
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(1f);
        TriggerDeath();
    }
}
