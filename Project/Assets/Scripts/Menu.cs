﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

    // Animation Handlers
    public Animator MainMenus;
    public Animator Tutorial;

    public void ClickStart()
    {
        //Load main game
        Application.LoadLevel("MAIN");
    }

    public void ClickQuit()
    {
        //Quit application
        Application.Quit();
    }

    // Tutorial button
    public void ClickTutorial()
    {
        MainMenus.SetBool("Tutorial", !MainMenus.GetBool("Tutorial"));
        Tutorial.SetBool("NotTutorial", !Tutorial.GetBool("NotTutorial"));
    }

    // Return button
    public void NotTutorial()
    {
        Tutorial.SetBool("NotTutorial", !Tutorial.GetBool("NotTutorial"));
        MainMenus.SetBool("Tutorial", !MainMenus.GetBool("Tutorial"));
    }
}
