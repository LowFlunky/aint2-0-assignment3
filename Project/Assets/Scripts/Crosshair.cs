﻿using UnityEngine;
using System.Collections;

/*
 * Author: Luke Taylor
 * Purpose: Draws crosshairs for the players in the game world
 */ 
public class Crosshair : MonoBehaviour
{
    // Crosshair Texture
    public Texture2D crosshairTexture;
    // It's scale (Testing Purposes, Unhide to use)
    [HideInInspector]
    public float crosshairScale = 1;

    // Update is called once per frame
    void OnGUI()
    {
        if (crosshairTexture != null)
        {
            GUI.DrawTexture(new Rect((Screen.width / 2 - crosshairTexture.width * crosshairScale) + (Screen.width / 4), (Screen.height - crosshairTexture.height * crosshairScale) / 2, crosshairTexture.width * crosshairScale, crosshairTexture.height * crosshairScale), crosshairTexture);
            GUI.DrawTexture(new Rect((Screen.width / 2 - crosshairTexture.width * crosshairScale) / 2, (Screen.height - crosshairTexture.height * crosshairScale) / 2, crosshairTexture.width * crosshairScale, crosshairTexture.height * crosshairScale), crosshairTexture);
        }
    }
}
