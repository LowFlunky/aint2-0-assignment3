﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Author: Richard Weeks, Edited by Luke Taylor
 * Purpose: Using Link List implementation to allow game entities to have a pooling system instead of instantiation and destroy.
 */
public class ObjectPooler : MonoBehaviour
{

    static ObjectPooler sInstance;

    //This class handles each pooled object, either creating a new one or reusing a free one
    class PoolObject
    {
        //Creates a new "Pool Object" to be added
        internal PoolObject nextPoolObject;
        //The GameObject to be created, from a prefab, if no pooled objects are free
        internal GameObject createdGameObject;
        //Counter used to determine the number of times the pooled object has been used for analytical testing
        internal int numTimesUsed;

        //Method called to create a new pool object from a prefab and return it
        internal static PoolObject Create(GameObject Prefab, PoolList Creator)
        {
            PoolObject NewPoolObject = new PoolObject();

            NewPoolObject.nextPoolObject = null;
            NewPoolObject.createdGameObject = GameObject.Instantiate(Prefab) as GameObject;
            NewPoolObject.createdGameObject.transform.parent = Creator.container.transform;
            NewPoolObject.numTimesUsed = 1;

            return (NewPoolObject);
        }

        //Method to Reuse a pooled object if available
        internal void Reuse(PoolList Creator)
        {
            if (createdGameObject != null)
            {
                createdGameObject.transform.parent = Creator.container.transform;
                numTimesUsed++;
            }
        }
    }

    //The open and closed Pool Lists to determine if an object is in use, reusable or a new one needs to be created
    class PoolList
    {
        //Container GameObject
        internal GameObject container;
        //Prefab that is used for this Pooled item
        internal GameObject prefab;
        //The used list of GameObjects
        internal PoolObject activePool;
        //The free list of available GameObjects to use
        internal PoolObject freePool;
        //An interal counter for the times each item is Reused, can be used for analytical data if preloading objects
        internal int numTimesReused;
        // counter for the times each item is Allocated which you can see in the hierachy
        internal int numTimesAllocated;

        //Default constructor for each prefab
        public PoolList(GameObject Prefab)
        {
            prefab = Prefab;
            activePool = null;
            freePool = null;
            numTimesReused = 0;
            numTimesAllocated = 0;

            container = new GameObject();
            container.name = Prefab.name;
            container.transform.parent = sInstance.gameObject.transform;
        }

        //Method to allocate a GameObject from the pool.
        internal GameObject AllocateFromPool()
        {
            PoolObject NewPoolObject;

            //If the pool doesn't exist then create one
            if (freePool == null)
            {
                numTimesAllocated++;
                NewPoolObject = PoolObject.Create(prefab, this);
                NewPoolObject.nextPoolObject = activePool;
                activePool = NewPoolObject;
            }
            else
            {
                numTimesReused++;
                NewPoolObject = freePool;
                NewPoolObject.Reuse(this);

                // Next free pool entry is now the head
                freePool = freePool.nextPoolObject;

                // Link node into active pool
                NewPoolObject.nextPoolObject = activePool;
                activePool = NewPoolObject;
            }

            //Set the created game object to active, and update its name by the number of times used
            if (NewPoolObject.createdGameObject != null)
            {
                NewPoolObject.createdGameObject.SetActive(true);
                NewPoolObject.createdGameObject.name = prefab + numTimesAllocated.ToString();
                return (NewPoolObject.createdGameObject);
            }
            else
            {
                numTimesAllocated++;
                NewPoolObject = PoolObject.Create(prefab, this);
                NewPoolObject.nextPoolObject = activePool;
                activePool = NewPoolObject;
                NewPoolObject.createdGameObject.SetActive(true);
                NewPoolObject.createdGameObject.name = prefab + numTimesAllocated.ToString();
                return (NewPoolObject.createdGameObject);
            }
        }

        //Release a GameObject from its current pool when it needs to move between pool or return false if active poolobject doesnt exist.
        internal bool Release(GameObject ThisGameObject)
        {
            PoolObject LastObject = null;
            PoolObject CheckObject = activePool;

            while (CheckObject != null)
            {
                if (CheckObject.createdGameObject == ThisGameObject)
                {
                    //Unlinke from active pool
                    if (LastObject == null)
                    {
                        activePool = CheckObject.nextPoolObject;
                    }
                    else
                    {
                        LastObject.nextPoolObject = CheckObject.nextPoolObject;
                    }

                    // Add to the free pool
                    CheckObject.nextPoolObject = freePool;
                    freePool = CheckObject;

                    ThisGameObject.SetActive(false);
                    return (true);
                }
                LastObject = CheckObject;
                CheckObject = CheckObject.nextPoolObject;
            }
            return (false);
        }
    }

    //Create the pool list and an array of pool lists to debug them
    List<PoolList> poolList;

    //On awake ensure the list is a singleton
    void Awake()
    {
        if (sInstance == null)
        {
            sInstance = this;
            poolList = new List<PoolList>();
            name = "_PoolManager";
        }
        else
            enabled = false;
    }

    //Create and return a gameObject from its prefab or allocate it from the pool
    GameObject CreateFromPrefab(GameObject Prefab)
    {
        for (int x = 0; x < poolList.Count; x++)
        {
            if (poolList[x].prefab == Prefab)
            {
                return (poolList[x].AllocateFromPool());
            }
        }
        PoolList NewPoolList = new PoolList(Prefab);

        //debugPoolArray[poolList.Count] = NewPoolList;
        poolList.Add(NewPoolList);
        return (NewPoolList.AllocateFromPool());
    }

    //Call the pool list release method to remove a gameObject from the pool list
    bool InternalRelease(GameObject ThisGameObject)
    {
        for (int x = 0; x < poolList.Count; x++)
        {
            if (poolList[x].Release(ThisGameObject) == true)
            {
                return (true);
            }
        }
        return (false);
    }

    //Ensure that each object in the hierachy is singular and call the Create From Prefab
    public static GameObject Create(GameObject Prefab)
    {
        if (sInstance == null)
        {
            GameObject singleton = new GameObject();

            sInstance = singleton.AddComponent<ObjectPooler>();
        }

        return (sInstance.CreateFromPrefab(Prefab));
    }

    //Call this objects release method in its pool list.
    public static bool Release(GameObject Object)
    {
        if (sInstance == null)
        {
            return (false);
        }
        return (sInstance.InternalRelease(Object));
    }

}
