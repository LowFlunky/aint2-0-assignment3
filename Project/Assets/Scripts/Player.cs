﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
 * Author: Luke Taylor
 * Purpose: Manages Player variables, shooting and reloading along with each players GUI and the win state.
 */
public class Player : GameEntity
{


    // Ammo Gui Texture
    public Texture2D ammoGUITexture;
    // Ammo Font
    public Font ammoFont;
    // Bullets to pool
    public GameObject bulletPrefab;
    // Light to turn on/off when fireing
    public Light gunshotLight;
    // Viewport for ammo GUI positioning
    public Camera playerCamera;
    // Players position
    public GameObject playerPos;
    // Player Health
    public int health;
    // Firing Point to ensure the player isn't hit by their own bullets (Could be a future feature)
    public GameObject firingPoint;
    // Reaload Audiosource
    public AudioSource reloadSound;
    // Walking AudioSource
    public AudioSource walkSound;
    // Animator
    public Animator animArea;
    //Reload Animator
    public Animator reloadAnim;
    // Win Canvas
    public Canvas winState;
    // Win Text
    public Text winText;
    //Other Player
    public GameEntity otherPlayer;

    // Win boolean
    public static bool winBool = false;

    // Shooting delay
    private float shotTimer = 0.1f;
    // Next fire timer
    private float nextFire = 0.0f;
    // Bullets left
    private int bulletsLeft;
    // Boolean when reloading (So the player cannot fire whilst reloading)
    private bool reloading;
    // Boolean to limit damage
    private bool damaged;
    // Check if Anim Delay Coroutine is running
    private bool runningRoutine;
    // Ammo rectangle (It goes around the font/label)
    private Rect ammoGui;
    // Color of the ammoGui Rectangle
    private Color ammoColor = new Color(1.0f, 1.0f, 1.0f, 0.30f);
    // Health Rectangle
    private Rect healthGui;
    // Color of the healthGui Rectangle
    private Color healthColor = new Color(1.0f, 0.2f, 0.2f, 0.30f);
    // Style to use for font
    private GUIStyle guiStyle = new GUIStyle();
    //Ammo and Reload switch text
    private string guiText;

    // Use this for initialization
    void Start()
    {
        Initialise();
    }   

    // Incase the game is resumed instead of reloaded
    void Awake()
    {
        Initialise();
    }

    // Actual initilisation (Incase Players are pooled)
    public override void Initialise()
    {
        Player.winBool = false;
        Time.timeScale = 1.0f;
        guiText = "Ammo";
        base.Initialise();
        runningRoutine = false;
        bulletsLeft = 30;
        health = 10;
        if (playerPos.gameObject.name == "Player1")
        {
            healthGui = new Rect(playerCamera.rect.width, Screen.height - (ammoGUITexture.height), ammoGUITexture.width, ammoGUITexture.height);
            ammoGui = new Rect((Screen.width / 2) - ammoGUITexture.width, Screen.height - (ammoGUITexture.height), ammoGUITexture.width, ammoGUITexture.height);
        }
        else
        {
            healthGui = new Rect(Screen.width - (Screen.width / 2), Screen.height - (ammoGUITexture.height), ammoGUITexture.width, ammoGUITexture.height);
            ammoGui = new Rect((Screen.width - ammoGUITexture.width), Screen.height - (ammoGUITexture.height), ammoGUITexture.width, ammoGUITexture.height);
        }
        reloading = false;
        damaged = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Quit back to menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("MENU");
        }

        //Win states
        if (health <= 0)
        {
            health = 0; // Ensure we dont get negative numbers
            Time.timeScale = 0.7f;
            winState.GetComponent<Canvas>().enabled = true;
            winText.text = playerPos.name.ToString() + " LOSES!";
            Player.winBool = true;

        }
        if (Player.winBool == false)
        {
            //Debug.Log(Input.GetAxis("Fire1Joy"));
            if (playerPos.gameObject.name == "Player1")
            {
                // When clicking and if there is an adequate delay, and if the player isn't reloading, then fire.
                if (Input.GetMouseButton(0) == true && Time.time > nextFire && bulletsLeft > 0 && reloading == false)
                {
                    animArea.SetBool("ShootingPlayer", true);
                    nextFire = Time.time + shotTimer;
                    Shoot();
                    if (runningRoutine == true)
                    {
                        StopCoroutine("AnimDelay");
                    }
                    StartCoroutine("AnimDelay");
                }

                // Reload
                if (Input.GetKeyDown(KeyCode.R) && reloading == false && bulletsLeft < 30)
                {
                    reloadSound.Play();
                    StartCoroutine("Reload");
                }
            }
            else
            {
                // When trigger is pulled and if the player isn't reloading, then fire.
                if (Input.GetAxisRaw("Fire1Joy") != 0 && Time.time > nextFire && bulletsLeft > 0 && reloading == false)
                {
                    animArea.SetBool("ShootingPlayer", true);
                    nextFire = Time.time + shotTimer;
                    Shoot();
                    if (runningRoutine == true)
                    {
                        StopCoroutine("AnimDelay");
                    }
                    StartCoroutine("AnimDelay");
                }


                // Reload
                if (Input.GetAxisRaw("SubmitJoy") > 0 && reloading == false && bulletsLeft < 30)
                {
                    reloadSound.Play();
                    StartCoroutine("Reload");
                }

            }


            // Whilst walking play walk sound
            if (playerPos.gameObject.name == "Player1")
            {
                if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D))
                {
                    walkSound.Play();
                    animArea.SetBool("MovingPlayer", true);
                }
                else if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
                {
                    walkSound.Stop();
                    animArea.SetBool("MovingPlayer", false);
                }
            }
            if (playerPos.gameObject.name == "Player2")
            {
                if (Input.GetAxisRaw("HorizontalJoy") != 0 || Input.GetAxisRaw("VerticalJoy") != 0)
                {
                    walkSound.Play();
                    animArea.SetBool("MovingPlayer", true);
                }
                else if (Input.GetAxisRaw("HorizontalJoy") == 0 && Input.GetAxisRaw("VerticalJoy") == 0)
                {
                    walkSound.Stop();
                    animArea.SetBool("MovingPlayer", false);
                }
            }
        }

    }

    // Using the Pool fire a new bullet (either reuse a free one or instantiate a new one), deduct from the bullet count
    private void Shoot()
    {
        gunshotLight.enabled = true;
        Vector3 newPos = new Vector3(firingPoint.transform.position.x, firingPoint.transform.position.y, firingPoint.transform.position.z);
        Vector3 force = transform.forward;
        Bullet.Create(this, bulletPrefab, newPos, force, playerCamera.transform);
        bulletsLeft -= 1;
        //Debug.Log("Bullets left: " + bulletsLeft);
        StartCoroutine("Light");
    }

    //If player is shot call appropriate method
    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag == "Bullet")
        {
            PlayerShot();
        }
    }

    // Hurt player unless they are currently in the state of damaged
    public void PlayerShot()
    {
        if (damaged == false)
        {
            health--;
            damaged = true;
            StartCoroutine("HurtDelay");
        }
    }

    // Reload, stop player from firing, after 2 seconds give the player 30 more bullets in their weapon and continue
    IEnumerator Reload()
    {
        guiText = "RELOADING!";
        reloading = true;
        reloadAnim.SetBool("reloading", true);
        yield return new WaitForSeconds(3.3f);
        reloadAnim.SetBool("reloading", false);
        bulletsLeft = 30;
        guiText = "Ammo";
        // Debug.Log("Reloaded");
        reloading = false;
    }

    // Simple light timer to make the weapon flash
    IEnumerator Light()
    {
        yield return new WaitForSeconds(0.2f);
        gunshotLight.enabled = false;
    }

    // Damage delay
    IEnumerator HurtDelay()
    {
        yield return new WaitForSeconds(0.2f);
        damaged = false;
    }

    IEnumerator AnimDelay()
    {
        //Debug.Log("Inside AnimDelay");
        runningRoutine = true;
        yield return new WaitForSeconds(1f);
        runningRoutine = false;
        animArea.SetBool("ShootingPlayer", false);
    }

    // GUI for the Ammo and health rectangle and Label
    #region GUI
    void OnGUI()
    {

        guiStyle.font = ammoFont;
        guiStyle.fontSize = 40;
        guiStyle.normal.textColor = Color.blue;
        guiStyle.border.Add(new Rect());
        GUI.color = ammoColor;
        GUI.DrawTexture(healthGui, ammoGUITexture);
        GUI.color = healthColor;
        GUI.DrawTexture(ammoGui, ammoGUITexture);
        GUI.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

        // Ammo Gui
        GUI.contentColor = Color.blue;
        GUI.Label(new Rect((healthGui.x + (healthGui.width) - 85f), (healthGui.y + (healthGui.height) - 80f), 150f, 150f), bulletsLeft.ToString() , guiStyle);
        GUI.Label(new Rect(healthGui.x, healthGui.y - (healthGui.height / 2), 150f, 150f), guiText, guiStyle);

        guiStyle.normal.textColor = Color.red;
        // Health Gui
        GUI.contentColor = Color.red;
        GUI.color = Color.red;
        GUI.backgroundColor = Color.red;
        GUI.Label(new Rect((ammoGui.x + (ammoGui.width) - 85f), (ammoGui.y + (ammoGui.height) - 80f), 150f, 150f), health.ToString(), guiStyle);
        GUI.Label(new Rect(ammoGui.x - ammoGui.width / 2, ammoGui.y - (ammoGui.height / 2), 150f, 150f), "Health", guiStyle);

    }
    #endregion

    #region Getters and Setters
    //Returns this objects position when called
    public Vector3 GetTransform()
    {
        return transform.position;
    }

    public int GetHealth()
    {
        return health;
    }
    #endregion
}
