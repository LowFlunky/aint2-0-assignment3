﻿using UnityEngine;
using System.Collections;

public class Debris : MonoBehaviour {

    public GameObject debris;

	void Start () {
        debris.rigidbody.AddForce(new Vector3(Random.Range(5f, 500f),Random.Range(5f, 500f),Random.Range(5f, 500f)));
	}
}
